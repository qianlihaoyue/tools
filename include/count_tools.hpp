/*
 * @Author: qianlihaoyue
 * @Description:计数器，结合时钟源可以实现灵活的软件定时器，可用于print debug以及嵌入式
 * @Editcnte: 2020-12-29 13:56:40
 * @Version: V1.0
 * @LastEditcnte: 2023-4-22
 * @Version: V1.1
 */
#pragma once

#include <unordered_map>

class Count {
   public:

    //计数循环
    static int Loop(const std::string& key, int setCount) {
        // 检查 key 是否存在于 cnt_blocks_ 中，如果不存在，则创建一个新的 Cnt 实例
        Cnt_STR& cnt = getOrCreateCnt(key);
        return CountLoop_(cnt,setCount);
    }
    //计数脉冲，达到计数值时会返回一个1的脉冲
    static int Pulse(const std::string& key, int setCount) {
        Cnt_STR& cnt = getOrCreateCnt(key);
        return CountPulse_(cnt,setCount);
    }
    //计数开关，达到计数值时,返回值变成1
    static int Switch(const std::string& key, int setCount) {
        Cnt_STR& cnt = getOrCreateCnt(key);
        return CountSwitch_(cnt,setCount);
    }
    //清除计数器
    static void Clear(const std::string& key) { 
        if (cnt_blocks_.find(key) != cnt_blocks_.end()) {
            Cnt_STR& cnt = cnt_blocks_[key];
            cnt.count=cnt.setCount=cnt.sta=0;
        }
    }

private:
    //计数状态枚举，默认状态为结束/空闲
    enum MyCountSta_ENUM{End,Run} ;
    //计数器结构体
    typedef struct {
        int count;    //循环计数
        int setCount; //设定计数
        int sta;      //计数状态
    } Cnt_STR;

    static std::unordered_map <std::string, Cnt_STR> cnt_blocks_;

    // 根据键获取或创建 Cnt 实例
    static Cnt_STR& getOrCreateCnt(const std::string& key) {
        if (cnt_blocks_.find(key) == cnt_blocks_.end()) {
            Cnt_STR new_cnt = {0, 0, End};
            cnt_blocks_[key] = new_cnt;
        }
        return cnt_blocks_[key];
    }

    static int CountLoop_(Cnt_STR &cnt, int setCount) {
        //计数压栈
        if (cnt.sta == End) {
            cnt.count = 0;           //清空计数
            cnt.sta = Run;           //设定状态
            cnt.setCount = setCount; //设定计数
        }
        //计数
        else if (cnt.sta == Run) {
            //达到计数值
            if (cnt.count >= cnt.setCount) {
                cnt.sta = End; //设定状态
                return 1;       //计数结束
            } else
                cnt.count++;
        }
        return 0;
    }

    static int CountPulse_(Cnt_STR &cnt, int setCount) {
        //计数结束或空闲
        if (cnt.sta == End) {
            //未设定计数，计数压栈
            if (cnt.setCount == 0) {
                cnt.count = 0;           //清空计数
                cnt.sta = Run;           //设定状态
                cnt.setCount = setCount; //设定计数
            }
        }
        //计数
        else if (cnt.sta == Run) {
            //达到计数值
            if (cnt.count >= cnt.setCount) {
                cnt.sta = End; //设定状态
                return 1;       //计数结束
            } else
                cnt.count++;
        }
        return 0;
    }

    static int CountSwitch_(Cnt_STR &cnt, int setCount) {
        //计数结束或空闲
        if (cnt.sta == End) {
            //未设定计数，计数压栈
            if (cnt.setCount == 0) {
                cnt.count = 0;           //清空计数
                cnt.sta = Run;           //设定状态
                cnt.setCount = setCount; //设定计数
            } else {
                return 1; //打开开关
            }
        }
        //计数
        else if (cnt.sta == Run) {
            //达到计数值
            if (cnt.count >= cnt.setCount) {
                cnt.sta = End; //设定状态
            } else
                cnt.count++;
        }
        return 0;
    }
};
std::unordered_map <std::string, Count::Cnt_STR> Count::cnt_blocks_;

/*
//循环计数不需要清除计数器，脉冲计数和开关计数再次使用时，需清除计数器
if(Count::Loop("He",5)){
    ROS_INFO("Hello ");
}
if(Count::Pulse("Pulse",15)){
    ROS_INFO("Pulse Once ");
}
if(Count::Switch("sw",20)){
    if(Count::Loop("Wo",10)){
        ROS_INFO("World ");
    }
}
*/