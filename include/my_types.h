#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>

/// alias for eigen
using Vec2d = Eigen::Vector2d;
using Vec2f = Eigen::Vector2f;
using Vec2i = Eigen::Vector2i;

using Vec3d = Eigen::Vector3d;
using Vec3f = Eigen::Vector3f;
using Vec3i = Eigen::Vector3i;
using Mat3d = Eigen::Matrix3d;
using Mat3f = Eigen::Matrix3f;

using Vec4d = Eigen::Vector4d;
using Vec4f = Eigen::Vector4f;
using Mat4d = Eigen::Matrix4d;
using Mat4f = Eigen::Matrix4f;

using Quatd = Eigen::Quaterniond;
using Quatf = Eigen::Quaternionf;

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <vector>

using PointType = pcl::PointXYZI;// PointXYZINormal; //PointXYZIRCT
using CloudType = pcl::PointCloud<PointType>;
using CloudPtr  = CloudType::Ptr;
using PointVector = std::vector<PointType, Eigen::aligned_allocator<PointType>>;