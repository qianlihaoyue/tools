#pragma once
#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <fstream>
#include <sstream>

// 是否让xyz归零
nav_msgs::Path ReadTumTrajFile(const std::string& file_path,bool xyz_tozero=true)
{
    nav_msgs::Path path;
    std::ifstream tum_file(file_path);
    std::string line;
    bool first_pose = true;
    geometry_msgs::Point first_position;
    first_position.x = first_position.y = first_position.z = 0.0;
    double timestamp, tx, ty, tz, qx, qy, qz, qw;

    if (tum_file.is_open())
    {
        path.header.stamp = ros::Time::now();
        path.header.frame_id = "camera_init";

        while (std::getline(tum_file, line))
        {
            std::istringstream iss(line);
            if (!(iss >> timestamp >> tx >> ty >> tz >> qx >> qy >> qz >> qw)) { break; }

            if (first_pose && xyz_tozero)
            {
                first_position.x = tx;first_position.y = ty;first_position.z = tz;
                first_pose = false;
            }

            geometry_msgs::PoseStamped pose_stamped;
            pose_stamped.header.stamp = ros::Time(timestamp);
            pose_stamped.pose.position.x = tx - first_position.x;
            pose_stamped.pose.position.y = ty - first_position.y;
            pose_stamped.pose.position.z = tz - first_position.z;
            pose_stamped.pose.orientation.x = qx;
            pose_stamped.pose.orientation.y = qy;
            pose_stamped.pose.orientation.z = qz;
            pose_stamped.pose.orientation.w = qw;

            path.poses.push_back(pose_stamped);
        }
        tum_file.close();
    }
    else
    {
        ROS_ERROR("Unable to open TUM trajectory file.");
    }

    return path;
}

void SaveTumTraj(const std::string &traj_file,nav_msgs::Path path_) {
    std::ofstream ofs;
    ofs.open(traj_file, std::ios::out);
    if (!ofs.is_open()) {
        std::cerr << "Failed to open traj_file: " << traj_file;
        return;
    }

    ofs << "#timestamp x y z q_x q_y q_z q_w" << std::endl;
    for (const auto &p : path_.poses) {
        ofs << std::fixed << std::setprecision(6) << p.header.stamp.toSec() << " " << std::setprecision(15)
            << p.pose.position.x << " " << p.pose.position.y << " " << p.pose.position.z << " " << p.pose.orientation.x
            << " " << p.pose.orientation.y << " " << p.pose.orientation.z << " " << p.pose.orientation.w << std::endl;
    }

    ofs.close();
}

// rviz 显示轨迹，通常会发送完整的轨迹，可使用 visualization_msgs::Marker LINE_LIST，而不是 LINE_STRIP, 建立一个增量轨迹

#include <std_msgs/ColorRGBA.h>
#include <visualization_msgs/Marker.h>

std_msgs::ColorRGBA DefaultColorFunction(const geometry_msgs::PoseStamped& point)
{
    std_msgs::ColorRGBA color;
    color.r = color.g = color.b = color.a = 1.0;
    return color;
}

void PubColoredTrajectory(const nav_msgs::Path& path, const ros::Publisher& pub_marker,
     const std::function<std_msgs::ColorRGBA(const geometry_msgs::PoseStamped&)>& getColor = DefaultColorFunction)
{
    visualization_msgs::Marker trajectory;
    trajectory.header = path.header;
    trajectory.ns = "colored_trajectory";
    // trajectory.id = 0;
    trajectory.type = visualization_msgs::Marker::POINTS; //LINE_STRIP
    trajectory.action = visualization_msgs::Marker::ADD;

    // 设置轨迹点的大小
    trajectory.scale.x = trajectory.scale.y = 0.1;

    // 添加轨迹中的点
    for (const auto& pose_stamped : path.poses){
        trajectory.points.push_back(pose_stamped.pose.position);
        trajectory.colors.push_back(getColor(pose_stamped));
    }
    
    // 发布轨迹标记
    pub_marker.publish(trajectory);
}