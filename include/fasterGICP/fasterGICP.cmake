# include(include/fasterGICP/fasterGICP.cmake)

# add_executable(icp_app src/icp_test.cpp)
# add_dependencies(icp_app fast_gicp)
# target_link_libraries(icp_app fast_gicp ${catkin_LIBRARIES} ${OpenMP_CXX_FLAGS})


include_directories(
    include/fasterGICP/include
    include/fasterGICP/include/gicp
)

find_package(PCL REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(OpenCV 4 REQUIRED)

find_package(OpenMP)
if (OPENMP_FOUND)
  set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()


catkin_package(
INCLUDE_DIRS include
LIBRARIES fast_gicp
)

add_library(fast_gicp SHARED
  include/fasterGICP/src/fast_gicp/gicp/lsq_registration.cpp
  include/fasterGICP/src/fast_gicp/gicp/fast_gicp.cpp
  include/fasterGICP/src/fast_gicp/gicp/fast_gicp_st.cpp
  include/fasterGICP/src/fast_gicp/gicp/fast_vgicp.cpp
)
target_link_libraries(fast_gicp
  ${PCL_LIBRARIES}
  ${OpenCV_INCLUDE_DIRS}
)
target_include_directories(fast_gicp PUBLIC
  include
  ${PCL_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIR}
  ${OpenCV_INCLUDE_DIRS} 
)
