#pragma once
// 帮我用C++写一个记录数据的代码,每个数据块由（数据名称，数据vector）构成，数据类型为double。
// 要求添加数据函数能多次调用，每次向vector插入一个数据,且有一个默认为1的形参，意味着调用该函数几次后才会真正添加数据。
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

class DataRecorder {
public:
    struct DataBlock {
        DataBlock() = default;
        DataBlock(const std::string& name) : data_name_(name), tmp_counter_(0),all_counter_(0) {}

        std::string data_name_;
        std::vector<double> data_vector_;
        int tmp_counter_;       // 临时变量
        int all_counter_;       // 记录所有调用次数
    };

    // 注意，同一个数据的counter_是公用的，当频次!=1时不要在多处对同一个数据进行写入
    static void AddData(const std::string& name, double data, int count_frequency = 1) {
        auto& data_block = data_blocks_[name];
        if (data_block.data_name_.empty()) 
            data_block.data_name_ = name;
        data_block.all_counter_++;
        data_block.tmp_counter_++;
        if (data_block.tmp_counter_ >= count_frequency) {
            data_block.data_vector_.push_back(data);
            data_block.tmp_counter_ = 0;
        }
    }

    static void PrintRecentData(const std::string& name, size_t recent_count) {
        auto it = data_blocks_.find(name);
        if (it != data_blocks_.end()) {
            const auto& data_block = it->second;
            std::cout << data_block.data_name_<< " : ";
            size_t start_index = data_block.data_vector_.size() > recent_count ? data_block.data_vector_.size() - recent_count : 0;
            for (size_t i = start_index; i < data_block.data_vector_.size(); ++i) {
                std::cout << data_block.data_vector_[i] << ' ';
            }
            std::cout << std::endl;
        } else {
            std::cerr << "Data not found for name: " << name << std::endl;
        }
    }

    static void PrintData(const std::string& name) {
        auto it = data_blocks_.find(name);
        if (it != data_blocks_.end()) {
            std::cout << it->second.data_name_ << " : ";
            for (const auto& value : it->second.data_vector_) {
                std::cout << value << ' ';
            }
            std::cout << std::endl;
        } else {
            std::cerr << "Data not found for name: " << name << std::endl;
        }
    }

    /// dump to a log file
    static void DumpIntoFile(const std::string& file_name) {
        std::ofstream ofs(file_name, std::ios::out);
        if (!ofs.is_open()) {
            std::cerr << "Failed to open file: " << file_name << "!!!"<< std::endl;
            return;
        } else {
            std::cout << "Dump Records into file: " << file_name<< std::endl;
        }

        ofs << ">>> ===== Printing data summary =====" << std::endl;
        // 设置列宽
        const int nameWidth = 30;
        const int scoreWidth = 15;
        
        // 输出表头
        ofs << std::left << std::setw(nameWidth) << "DataName"
            << std::setw(scoreWidth) << "Mean"      
            << std::setw(scoreWidth) << "S.D"      
            << std::setw(scoreWidth) << "All times"  // 调用的所有次数 
            << std::setw(scoreWidth) << "Data size"  // 记录的次数
            << std::endl;
        // 输出分隔线
        ofs << std::setfill('-') << std::setw(nameWidth + 3 * scoreWidth) << "" << std::setfill(' ') << std::endl;
        
        //遍历计算每一项
        for (auto& r : data_blocks_) {
            std::vector<double> &datas = r.second.data_vector_;
            //均值
            double ave_data=std::accumulate(datas.begin(), datas.end(), 0.0) / double(datas.size());

            // 计算方差
            double variance = 0.0;
            for (const auto& data : datas) 
                variance += (data - ave_data) * (data - ave_data);
            variance /= datas.size();

            ofs << std::fixed << std::setprecision(3) << std::left 
                << std::setw(nameWidth) << r.first
                << std::setw(scoreWidth) << ave_data
                << std::setw(scoreWidth) << sqrt(variance)
                << std::setw(scoreWidth) << r.second.all_counter_
                << std::setw(scoreWidth) << datas.size()
                << std::endl;
        }
        ofs << ">>> ===== Printing data summary end =====" << std::endl;

        size_t max_length = 0;
        for (const auto& kv : data_blocks_) {
            ofs << std::setw(scoreWidth) << kv.first ;
            max_length = std::max(max_length, kv.second.data_vector_.size());
        }
        ofs << std::endl;

        for (size_t i = 0; i < max_length; ++i) {
            for (const auto& kv : data_blocks_) {
                const auto& data_block = kv.second;
                if (i < data_block.data_vector_.size()) 
                    ofs << std::setw(scoreWidth) << data_block.data_vector_[i];
                else 
                    ofs << std::setw(scoreWidth) << "-";
                ofs << std::setw(scoreWidth);
            }
            ofs << std::endl;
        }
        ofs.close();
    }

    static void Clear() { data_blocks_.clear(); }

private:
    static std::map<std::string, DataBlock> data_blocks_;
};

//无序图会不会导致数据对不上
// 想不定义对象直接引用，可以声明函数为 static 静态的
std::map<std::string, DataRecorder::DataBlock> DataRecorder::data_blocks_;