#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>

#include "time_tools.hpp"
#include "record_tools.hpp"
#include <omp.h> 
#include <execution>
#include <tbb/tbb.h>

int f_final(int n) {
    if(n < 2) return n;
    return f_final(n-1) + f_final(n-2);
}

void Run1(){
    int sum=0;
    for (int j = 0; j < 100; j++)
    {
        Timer::Evaluate("fun1", [&](){
            f_final(30);
            sum++;
        });
    }
    // std::cout<<sum<<std::endl; 
}

void Run2(){
    int sum=0;
    #pragma omp parallel for reduction(+:sum)
    for (int j = 0; j < 100; j++)
    {
        Timer::Evaluate("fun2", [&](){
            f_final(30);
            sum++;
        });
    }
    // std::cout<<sum<<std::endl;        
}

void Run3(){
    int sum=0;
    std::vector<int> obj;
    for (int j = 0; j < 100; j++)
        obj.emplace_back(j);

    tbb::mutex sum_mutex; // 添加一个互斥锁来保护对 sum 的访问
    std::for_each(std::execution::par_unseq, obj.begin(), obj.end(), [&](const size_t &i) {
        Timer::Evaluate("fun3", [&](){
            f_final(30);
            tbb::mutex::scoped_lock lock(sum_mutex); // 使用互斥锁保护 sum 的修改
            sum++;
        });
    });
    // std::cout<<sum<<std::endl;        
}

void TimeTest(){
    Timer::Evaluate("Run1", [&](){
        Timer::Start("Run1_d");
        Run1();
        Timer::End("Run1_d");
    });
    Timer::Evaluate("Run2", [&](){
        Run2();
    });
    Timer::Evaluate("Run3", [&](){
        Run3();
    });
    // Timer::PrintAll();
    Timer::DumpIntoFile(std::string(ROOT_DIR)+"Log/time.txt",true);
}

void DataTest(){
    // DataRecorder logger;
    for(int i=0;i<20;i++){
        DataRecorder::AddData("double_data", 2.0, 2);   
        DataRecorder::AddData("float_data", 4.0, 4);   
    }
    DataRecorder::PrintRecentData("double_data",4);
    DataRecorder::PrintData("float_data");
    DataRecorder::DumpIntoFile(std::string(ROOT_DIR)+"Log/data.txt");
}

int main(int argc, char** argv)
{
    // DataTest();
    TimeTest();

    return 0;
}


