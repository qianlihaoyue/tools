#include <iostream>
#include <vector>

#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include "traj_tools.hpp"

#include "count_tools.hpp"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "tools");
    ros::NodeHandle nh;
    ros::Publisher path_pub = nh.advertise<nav_msgs::Path>("path", 100000);
    ros::Publisher pub_marker = nh.advertise<visualization_msgs::Marker>("path_color", 10);

    std::string gt_path_;
    nh.param<std::string>("gt_path", gt_path_, "gt_path.txt");
    nav_msgs::Path path = ReadTumTrajFile(gt_path_,true);
    
    path.header.stamp = ros::Time::now();
    path.header.frame_id = "camera_init";

    ros::Rate rate(5);
    while (ros::ok()) {

        ros::spinOnce();

        if(Count::Loop("He",5)){
            ROS_INFO("Hello ");
        }
        if(Count::Pulse("Pulse",15)){
            ROS_INFO("Pulse Once ");
        }
        if(Count::Switch("sw",20)){
            if(Count::Loop("Wo",10)){
                ROS_INFO("World ");
            }
        }

        path_pub.publish(path);
        PubColoredTrajectory(path,pub_marker);

        rate.sleep();
    }
    return 0;
}


