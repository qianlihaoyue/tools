#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64MultiArray.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <Eigen/Dense>


using namespace Eigen;
using namespace std;

class VariationalBayesLinearRegression {
public:
    VariationalBayesLinearRegression(double alpha, double beta)
    : alpha(alpha), beta(beta), S_inv(Matrix3d::Zero()), mu(Vector3d::Zero()) {}

    void update(const Vector3d &point) {
        Vector3d phi = Vector3d(point[0], point[1], 1.0);
        S_inv += beta * phi * phi.transpose();
        mu += beta * point[2] * phi;

        Matrix3d S = S_inv.inverse();
        mu = S * mu;

        cout << "mu:" <<mu[0] << mu[1] << mu[2] << endl;


    }

    void get_params(Vector4f &params) {
        Vector3d normvec;
        normvec << mu[0], mu[1], -1.0;
        double n = normvec.norm();

        params << normvec(0) / n, normvec(1) / n, normvec(2) / n, mu[2] / n;
    }

    void get_params(Vector3d &params) {
        params = mu;
    }

private:
    double alpha;
    double beta;
    Matrix3d S_inv;
    Vector3d mu;
};




int main(int argc, char** argv) {
    ros::init(argc, argv, "plane_fitting_node");
    ros::NodeHandle nh;

    // Initialize the prior parameters: alpha (precision of prior) and beta (precision of likelihood)
    double alpha0 = 1e-6;
    double beta0 = 1.0;

    // Initialize the variational bayes linear regression object
    VariationalBayesLinearRegression vb(alpha0, beta0);

    // Input 3D points
    vector<Vector3d> points = {
        {1.0, 2.0, 3.0},
        {2.0, 3.0, 4.0},
        {3.0, 4.0, 5.0},
        {2.5, 3.5, 4.5},
        {3.0, 3.0, 3.0}
    };


    ros::Rate loop_rate(1); // 1 Hz
    while (ros::ok()) {

        // Update the parameters for each point
        for (const auto &point : points) {
            vb.update(point);
        }

        Vector4f params;
        vb.get_params(params);
        cout << params[0] << params[1] << params[2] << params[3] << endl;

        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}


