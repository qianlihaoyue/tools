#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

#include "time_tools.hpp"

#include "my_types.h"

#include <pcl/filters/crop_box.h>
CloudPtr cropPointCloud(const CloudPtr& input_cloud, const Vec3f& min_point, const Vec3f& max_point) {
    // 定义立方体空间
    pcl::CropBox<PointType> crop_box;
    crop_box.setInputCloud(input_cloud);

    crop_box.setMin(Vec4f(min_point[0], min_point[1], min_point[2], 1.0));
    crop_box.setMax(Vec4f(max_point[0], max_point[1], max_point[2], 1.0));

    CloudPtr local_cloud(new CloudType);
    crop_box.filter(*local_cloud);

    return local_cloud;
}

void pub_local_map(const ros::Publisher pub,CloudPtr local_cloud){
    sensor_msgs::PointCloud2 local_cloud_msg;
    pcl::toROSMsg(*local_cloud, local_cloud_msg);
    local_cloud_msg.header.frame_id = "map"; // 设置坐标系
    local_cloud_msg.header.stamp = ros::Time::now();
    pub.publish(local_cloud_msg);
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "local_map_node");
    ros::NodeHandle nh;

    std::string prior_map_path_;
    nh.param<std::string>("prior_map_path", prior_map_path_, "prior_map_path.txt");

    auto t1 = std::chrono::high_resolution_clock::now();
    // 读取点云文件
    CloudPtr cloud(new CloudType());
    if (pcl::io::loadPCDFile<PointType>(prior_map_path_, *cloud) == -1) {
        ROS_ERROR("无法读取PCD文件 %s", prior_map_path_.c_str());
        return -1;
    }
    auto t2 = std::chrono::high_resolution_clock::now();
    double time_used = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count() * 1000;    
    ROS_INFO("time_used:%.2f ",time_used);
    
    ros::Publisher local_map_pub = nh.advertise<sensor_msgs::PointCloud2>("local_map", 1);

    // usleep(1000);
            ros::Duration delay(1.0);  // 延迟1秒,等待rviz启动
            delay.sleep();
            pub_local_map(local_map_pub,cloud);
            ROS_INFO("load success");
            
    bool first_scan = true;
    int cnt=0;
    // float cnt=0.0;
    ros::Rate loop_rate(1); // 1 Hz
    while (ros::ok()) {

        ROS_INFO("cnt:%d",cnt++);

        if(first_scan){
            first_scan = false;

        }

        


        // cnt+=1.0;

        // 设置立方体的最小点和最大点
        // Vec3f min_point(cnt, cnt, -5.0);
        // Vec3f max_point(100.0+cnt, 100.0+cnt, 50.0);
        // CloudPtr local_cloud = cropPointCloud(cloud,min_point,max_point);

        

        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
