## Update

- 4.8  - 1.2 更新DateRecorder为静态
- 4.8  - 1.3 添加时间记录顺序
- 4.10 - 1.4 改为rospkg，测试轨迹
- 4.15 - 1.5 添加FasterGICP打包
- 4.22 - 1.6 添加计数器
- 6.21 - 1.7 更新time_tools
